# Notch Clock Offset

When running Asahi Linux on a Macbook with a notch camera, you can enable
`apple_dcp.show_notch=1` as boot commandline. This way the whole screen will
be used, but gnome-shell is not aware of the 'notch'. This is the part of the
screen that does not display anything but hosts the webcam of the device.

This extension allows to offset the clock by a few percent so it is no longer
hidden behind the notch and becomes usable again.

# Generating .pot file

    xgettext *.js
    sed -i \
        -e "s:SOME DESCRIPTIVE TITLE:Notch Clock Offset:" \
        -e "s:YEAR THE PACKAGE'S COPYRIGHT HOLDER:2023:" \
        -e "s:PACKAGE:gnome-shell-extension-notch-clock-offset:" \
        -e "s:FIRST AUTHOR <EMAIL@ADDRESS>, YEAR:Christoph Brill <opensource@christophbrill.de>, 2023:" \
        messages.po
    mv messages.po locale/gnome-shell-extension-notch-clock-offset.pot

## Building

    mkdir build
    pushd build
    meson ..
    ninja
    cp schemas/* ../schemas
    popd

## Packaging

    export VERSION="X"
    jq ".version = ${VERSION}" metadata.json > metadata.new.json && mv metadata.new.json metadata.json
    sed -i -e "s/^  version: '.*',$/  version: '${VERSION}',/" meson.build
    git commit metadata.json -m "release: ${VERSION}"
    git tag -a -m "v${VERSION}" v${VERSION}
    zip notch-clock-offset@christophbrill.de.v${VERSION}.shell-extension.zip metadata.json *.js schemas/gschemas.compiled schemas/*.xml