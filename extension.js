/* extension.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

/* exported init */

import {panel} from 'resource:///org/gnome/shell/ui/main.js';
import {Extension, gettext as _} from 'resource:///org/gnome/shell/extensions/extension.js';

let SIDE = 'right';

export default class NotchClockOffsetExtension extends Extension {

    _onSettingsChange() {
        // Get the 'side' from settings, which is enforced to be either 'left' or 'right'
        let side = this._settings.get_string('side');
        if (side != 'left') {
            side = 'right';
        }

        // If the value changed, disable any possible previous margin and enable the new margin
        if (SIDE != side) {
            this._disable();
            SIDE = side;
            this.enable();
            console.debug(_(`Moved clock to ${SIDE} of the notch`));
        }
    }

    _enable() {
        // Get the main panels width
        let panelWidth = panel.width;

        // If the user wants to move the clock to the left, add a right margin, otherwise add a left margin
        if (SIDE == 'left') {
            panel.statusArea.dateMenu.style = 'margin-right: ' + Math.round(panelWidth/10) + 'px';
        } else {
            panel.statusArea.dateMenu.style = 'margin-left: ' + Math.round(panelWidth/10) + 'px';
        }
    }

    _disable() {
        // Disable the margins set by the extension
        if (SIDE == 'left') {
            panel.statusArea.dateMenu.style = 'margin-right: 0';
        } else {
            panel.statusArea.dateMenu.style = 'margin-left: 0';
        }
    }

    enable() {
        // Load the settings if available
        this._settings = this.getSettings();
        this._settingsChangedId = this._settings.connect('changed', this._onSettingsChange.bind(this));
        this._onSettingsChange();
        this._enable();
    }

    disable() {
        this._disable();
        this._settings = null;
    }
}
