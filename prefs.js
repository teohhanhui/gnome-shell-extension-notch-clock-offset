/* prefs.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

'use strict';

import Adw from 'gi://Adw';
import Gio from 'gi://Gio';
import Gtk from 'gi://Gtk';
import GObject from 'gi://GObject';
import {ExtensionPreferences, gettext as _} from 'resource:///org/gnome/Shell/Extensions/js/extensions/prefs.js';

// Create our own class to forward the index 'selected' as textual
// representation
const ComboRowSelected = GObject.registerClass({
    GTypeName: 'ComboRowSelected',
    Properties: {
        'selectedText': GObject.ParamSpec.string(
            'selectedText',
            'Selected text',
            'The textual representation for selected',
            GObject.ParamFlags.READWRITE,
            ''
        ),
    },
}, class ComboRowSelected extends Adw.ComboRow {
    constructor(constructProperties = {}) {
        super(constructProperties);
        this.connect('notify::selected-item', () => {
            // Whenever the selected-item is signaled, also signal
            // 'selectedText' to trigger the settings bind
            this.notify('selectedText');
        });
    }

    // Getter invoked by the settings bind
    get selectedText() {
        return this.model.get_item(this.selected).get_string();
    }

    // Setter invoked by the settings bind
    set selectedText(value) {
        for (let i = 0; i < this.model.get_n_items(); i++) {
            const existing = this.model.get_item(i).get_string();
            if (existing == value) {
                this.selected = i;
                return;
            }
        }
    }
});

export default class NotchClockOffsetExtensionPreferences extends ExtensionPreferences {
    fillPreferencesWindow(window) {
        const settings = this.getSettings();

        // Create the preferences page, with a single group
        const page = new Adw.PreferencesPage();
        window.add(page);
        const group = new Adw.PreferencesGroup();
        page.add(group);

        // Create a model and add the single preferences row
        const model = new Gtk.StringList();
        model.append('left');
        model.append('right');
        const row = new ComboRowSelected();
        row.set_title(_('Move clock to side of notch'));
        row.set_use_subtitle(false);
        row.set_model(model);
        row.selectedText = settings.get_string('side');
        group.add(row);

        // Create a text entry and bind it to the text buffer
        settings.bind('side', row, 'selectedText', Gio.SettingsBindFlags.DEFAULT);

        // Make sure the window doesn't outlive the settings object
        window._settings = settings;
    }
}